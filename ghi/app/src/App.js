import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CodeEditorWindow from './CodeEditorWindow';
function App() {
  return (
    <CodeEditorWindow/>
  );
}

export default App;
