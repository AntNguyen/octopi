import React, { useState,useRef } from "react";
import Editor from "@monaco-editor/react";
import apikey from "./keys";

function testmaker(funcName,anInput,result,src){
  let solution =
  `
${src}
message = "${anInput} does not return ${result}"
assert ${funcName}(${anInput}) == ${result},message
print("test passed!")`
  return solution
}

function CodeEditorWindow(){
  const [showload,setShowload] = useState(false)
  const [apiResponse, setApiResponse] = useState(null)
  const editorRef = useRef(null)
  const [resultColor,setResultColor] = useState("rounded-3 bg-secondary-subtle")
  const [testForm,setTestForm] = useState({
    funName:"",
    description:"",
    input:"",
    result:""
  })
  const [testDescription,setTestDescription] = useState("Default Question")
  let getResult = async(getUrl)=>{
    let fetchOptions = {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'Content-Type': 'application/json',
        'X-RapidAPI-Key': apikey,
        'X-RapidAPI-Host': 'judge0-ce.p.rapidapi.com'
      }
    }
    try{
      let getResponse = await fetch(getUrl,fetchOptions)
      if (getResponse.ok){
        let getdata = await getResponse.json()
        let statusId = getdata.status_id
        if (statusId === 1 || statusId === 2){
          setTimeout(()=>{
            getResult(getUrl)
          },2000)
          return
        } else {
          if(getdata.stderr){
            setResultColor("rounded-3 bg-danger-subtle")
            setApiResponse(atob(getdata.stderr))
          }else setApiResponse(atob(getdata.stdout))
          return
        }
      }
    }catch(err){
      console.log("err",err)
    }
  }

  function handleEditorMounted(editor,monaco){
    editorRef.current = editor
  }

  let handleButtonClick = async() => {
    setResultColor("rounded-3 bg-secondary-subtle")
    let inside = testmaker(testForm.funName,testForm.input,testForm.result,editorRef.current.getValue())
    console.log(inside)
    let code64 = btoa(inside)
    const url = 'http://localhost:2358/submissions?base64_encoded=true&fields=*'
    const options = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        'Content-Type': 'application/json',
        'X-RapidAPI-Key': apikey,
        'X-RapidAPI-Host': 'judge0-ce.p.rapidapi.com'
      },
      body: JSON.stringify({
        language_id: 71,
        source_code: code64
      })
    }
    try {
      const response = await fetch(url, options)
      if (response.ok){
        const result = await response.json()
        let getUrl = `http://localhost:2358/submissions/${result.token}?base64_encoded=true&fields=*`
        getResult(getUrl)
      }else {
        setResultColor("rounded-3 bg-danger-subtle")
        setApiResponse("submission error")
        console.log("error")}
    } catch (error) {
      console.error(error)
    }
  }

  let handleFormSubmit= (event) => {
    event.preventDefault()
    console.log(testForm)
    setTestDescription(testForm.description)
  }

  let handleFormChange = (event) =>{
    let value = event.target.value
    let key = event.target.name
    setTestForm({
      ...testForm,
      [key]:value
    })
  }

  return(
    <>
      <div className="my-5 container">
        <div className="offset-3 col-6 rounded-3">
          <div className="shadow p-4 mt-4">
            <h2>Add a coding test</h2>
            <form onSubmit={handleFormSubmit} id="formtest">
              <div className="mb-3">
                <label htmlFor="funName">Function Name</label>
                <input value={testForm.funName} onChange={handleFormChange} placeholder="Function Name...Just name please" required type="text" id="funName" className="form-control"
                  name="funName" />
              </div>
              <div className="mb-3">
                <label htmlFor="Description" className="form-label">Description</label>
                <textarea onChange={handleFormChange} value={testForm.description} className="form-control" id="description" rows = "2" name="description"></textarea>
              </div>
              <div className="mb-3 input-group">
                <span className="input-group-text">Input and Results</span>
                <input onChange={handleFormChange} value={testForm.input} placeholder="Input" type="text" aria-label="Input" className="form-control" name="input"/>
                <input onChange={handleFormChange} value={testForm.result} placeholder="Expected Result" type="text" aria-label="Output" className="form-control" name="result"/>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

      <div className="offset-3 col-6 rounded-3 bg-success-subtle" >
        <div className="shadow p-4 mt-4">
          <div className="header">{testDescription}</div>
          <div className="body">
            <Editor
              height="50vh"
              defaultLanguage="python"
              defaultValue=""
              theme="vs-dark"
              onMount={handleEditorMounted}
            />
            <div className="d-flex flex-row-reverse">
              <button onClick={()=>{handleButtonClick();setShowload(true);setApiResponse("loading")}} className="btn btn-secondary">Run</button>
            </div>
          </div>
        </div>
        {showload ? <div className={resultColor}>{apiResponse}</div>:null}
      </div>
    </>
  )
}
export default CodeEditorWindow
